This package provides the basic interfaces used by the framework. It manages getting errors from any spot in your application to the running error reporter.

#### Using the Error Reporter

##### Launching an Error Reporter

The framework routes all errors to a global error reporter. This error reporter is defined by overriding `Error Reporter.lvclass` class, and then calling `Create.vi`.

Search GPM for `Error Reporter` to find pre-built error reporters.

##### Reporting an Error

Use `Report Error.vi` anywhere in your application to send an error the reporter.

### MGI Error Reporter Framework

The MGI Error Reporter Framework is a high level tool to help catch and report errors. It is designed to be extremely modular and flexible, so it can be tweaked for your application.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
